# 2024-04

## Goals

### Planned

- Find potential improvements in planning repo.
    - Should you avoid goal duplicates by using some kind of Goal id?
    - Should you state how much time did each goal take?

### Done

- Ask for feedback about two MRs
    - MR1: https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/490
    - MR2: https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/491
- Learn how to make good commits
    - Used Resource: https://optimizedbyotto.com/post/good-git-commit/
- Squash MR1's commits using `git rebase` command.
- Improve MR1's commit message.
- Complete GSoC Proposal
    - Ask for feedback after finishing it.
    - Submit it.
    - Proposal link: https://wiki.debian.org/AhmedSiam/Gsoc2024Proposal

### Canceled


## Logs

### 2024-04-02

- Time: 4H, 20M

### Done

- Ask for feedback about two MRs
    - MR1: https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/490
    - MR2: https://salsa.debian.org/salsa-ci-team/pipeline/-/merge_requests/491
- Learn how to make good commits
    - Used Resource: https://optimizedbyotto.com/post/good-git-commit/
- Squash MR1's commits using `git rebase` command.
- Improve MR1's commit message.
- Complete GSoC Proposal
    - Ask for feedback after finishing it.
    - Submit it.
    - Proposal link: https://wiki.debian.org/AhmedSiam/Gsoc2024Proposal

### Notes

- MR2 got closed as it's change appeared to be unneeded.
