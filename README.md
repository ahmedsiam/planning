# Salsa Planning

- Each month's goals and logs would be stored in a file.
- There should be one commit per day.
    - In case of having multiple commits in a day,
      It should be squashed into one commit


## Goals

SMART Goals added and marked as finished or canceled with a
file format of:

```md
### Planned
- Goal 3

### Done
- Goal 1

### Canceled
- Goal 2
```

## Logs

Daily logs with a file format of:

```md
## YYYY-MM-DD
- Time: XH, YM (H for Hours and M for Minutes)
### Done (Done during this time)
- Goal 1
- Goal 2
- etc
### Notes (Optional)
- Note 1
- Note 2
- etc
```
